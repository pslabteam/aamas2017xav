
\section{Design and implementation}
\label{sec:design}

The model introduced on section~\ref{sec:proposal} is open enough to allow different implementations. This section presents a practical possibility, intended to be integrated with JACAMO. The core implementation and main design choices are related to the following aspects: agent API, infrastructure artifacts, deployment, and connection loss; which are discussed next.     

\subsection{Agent API}
\label{sec:agAPI}

The agent API provides the means for agents, and agent plan programmers, to exploit the distributed environment through workspace related perceptions, events, actions, and internal actions to ease topology lookup; maintaining the goals of distributed transparency.  In the descriptions that follow, a $+$ sign on the parameter list means an input, while $-$ an output. 

 Perceptions:

\begin{itemize}
\item $?workspaces(-ListWsp)$: a list of all the workspaces that the agent has joined.
Example usage:
\begin{lstlisting}
  ?workspaces(WspList);
\end{lstlisting}
\item $?workspacesTree(-Tree)$: a full topology of the current workspace tree which can be traversed. $Tree$ is a list of the form:\\
$[root, [subNode_1, [subsubNode_1, [...], subsubNode_m]], ...,$\\
 $[subNode_n]]$. Example usage:
\begin{lstlisting}
  ?workspacesTree([Root | Children]);
  println("Root workspace: ", Root);
\end{lstlisting}
\item $?home(-Wsp)$: the home workspace of an agent. The home is the original workspace where the agent is deployed. The home can not change. Example usage:
\begin{lstlisting}
  ?home(Home);
  println("This is my home ", Home);
\end{lstlisting}
\end{itemize}



Events:

\begin{itemize}
\item $joined(-Wsp, -Agent)$: for agents to know that other agents joined a workspace. Limited only to agents on same workspaces. Example usages:
  \begin{lstlisting}
    +joined("$home/room1", Ag) : true
       <- println("Hello ", Ag).
  \end{lstlisting}

\item $leaved(-Wsp, -Agent)$: inverse of previous one. Example usages:
  \begin{lstlisting}
    +leaved("$home/room1", Ag) : true
       <- println("Goodbye ", Ag).
  \end{lstlisting}
\item $mounted(-Wsp)$: for agents to know that a new workspace has been mounted.
Example usages:
  \begin{lstlisting}
    +mounted(Wsp) : true
       <- joinWsp(Wsp).
  \end{lstlisting}
\item $unmounted(-Wsp)$: inverse of previous one. Example usages:
  \begin{lstlisting}
    +unmounted(Wsp) : true
       <- quitWsp(Wsp).
  \end{lstlisting}
\item $dropped(-Wsp)$: triggered when a workspace drops as a result of an  issue. It is different than unmounted in the sense that a drop connection is an unexpected error, so error handling is required.
 Example usages:
  \begin{lstlisting}
    +dropped(Wsp) : true
       <- quitWsp(Wsp).
  \end{lstlisting}
\item $reconnected(-Wsp)$: triggered when a workspace is successfully reconnected after dropping. It is different than mounted, it implies an error recuperation. Example usages:
  \begin{lstlisting}
    +mounted(Wsp) : true
       <- joinWsp(Wsp).
  \end{lstlisting}
\end{itemize}

Actions:

\begin{itemize}
\item $joinWsp(+Wsp)$: the agent adds a specified workspace to its joined workspaces list. Following definition~\ref{def:model}, wsp is a path that relates to the workspace tree. As in UNIX file system paths, this path can also contain special modifiers to allow relative references, this includes: $\$home$ to refer to the home workspace of the agent, and $\$root$ to refer to the root workspace on the tree. Example usages:
  \begin{lstlisting}
    joinWsp("main/house1/bathroom");
    //Another option
    joinWsp("$root/house1/bathroom");
    //A third option if home = main/house1
    joinWsp("$home/bathroom"); 
  \end{lstlisting}
\item $quitWsp(+Wsp)$: removes a specified workspace from its workspace list.  Example usage:
  \begin{lstlisting}
    quitWsp("main/house1/bathroom");
  \end{lstlisting}
\item $mount(+MountPoint)$: creates a new workspace and mounts it on the specified path. The workspace will actually be spawned on the same CArtAgO node as the parent workspace derived from $MountPoint$, this allows workload management for workspaces. Example usage:
  \begin{lstlisting}
    mount("$root/house2");
  \end{lstlisting}
\item $unmount(+MountPoint)$: unmounts a
workspace and its children workspaces, reforming the workspaces tree.
 Example usage:
  \begin{lstlisting}
    unmount("$root/house2");
  \end{lstlisting}
\end{itemize}

Internal actions:

\begin{itemize}
\item $parentWsp(+WspIn, -WspOut)$: returns the parent workspace for a given workspace.  Example usage:
  \begin{lstlisting}
    parentWsp("main/house1", Parent);
  \end{lstlisting}
\item $childrenWsps(+WspParent, -WspsList)$: returns the children workspaces of a given workspace. Example usage:
  \begin{lstlisting}
    childrenWsps("$root", ChildrenList);
  \end{lstlisting} 
\item $searchPaths(+PointPath, +RegExp, -WspsList)$: returns a list with the workspaces that follow a certain regular expression, the search is restricted to the subtree given by $PointPath$.  Example usage:
  \begin{lstlisting}
    searchPaths("$home", ".*/bath.*", List);
  \end{lstlisting} 
\end{itemize}



\subsection{Infrastructure artifacts}
\label{sec:artifacts}

Following the CArtAgO way, the agent API, presented in section~\ref{sec:agAPI}, is supported by environment artifacts. Such artifacts are special in the sense that they are always present, and agents automatically focus on them. A single kind of artifact is implemented for the agent API: NodeArtifact. The mentioned artifact is already present in the current implementation of CArtAgO, however, some important changes are required to follow the proposed model.

Nodes represent CArtAgO processes, possibly remote, running on a given host:port. Nodes are the main abstraction to manage workspaces, and as such, they provide all the necessary tools to create, dispose, join, and quit workspaces, as well as the means to communicate with other nodes in order to maintain a consistent workspace tree, and properly dispatch events. A NodeArtifact is the gateway used by an agent to interact with the node services and to observe the distributed environment. There is a NodeArtifact in each node, and every agent has access to one of them, which one depends on its $\$home$ workspace, which in turn it is intended to be on the same node as the agent process. 

Nodes communicate between each other following a centralized approach: one node is designated as the central node, so every change on the workspaces is inspected and approved by a single node, and the associated actions and events are dispatched from there. This centralized approach makes possible to maintain a consistent workspace tree and to efficiently propagate events. To exemplify node communication, the workflow for mounting a workspace is the following:
\begin{itemize}
\item An agent that wants to mount a workspace issues the action to its corresponding NodeArtifact passing a mount point.
\item The artifact sends a request to the central node.
\item The central node checks if the mount point is consistent with the workspace tree, if it is, then it issues a request to the end node where the workspace is actually going to exist.
\item The end node creates the workspace and returns control to the central node.
\item The central node makes the corresponding changes to the workspace tree and communicates the success to the original requesting node. It also dispatches a mount and tree change events to the other nodes. 
\end{itemize}

As the node model is centralized, there exists the concern of a single point of failure, that's why all nodes maintain redundant information about the environment, so it is possible to recuperate from a central node dropping. This is further discussed on section~\ref{sec:dropping}.

\subsection{Deployment}
\label{sec:deployment}

One of the goals of the proposed implementation is to ease the deployment of the distributed system, this means, to be able to configure and launch the desired hosts, nodes, and workspaces that will take part in the MAS from the start. The proposed deployment extends the one from JACAMO, where only workspaces are considered. JACAMO uses a text file known as the JCM file to configure the deployment of the MAS. The intention is to further include fields in this file to also configure host, and nodes for distributed systems; and add the facilities to automatically launch CArtAgO nodes.

The changes to the JCM file include:
\begin{itemize}
\item Host configuration: assign a logical name and IP address to each host.
\item Node configuration: assign a logical name for the node, i.e.; the name of the default workspace; the related host name; and optionally a port number.
\item Workspaces configuration: relate each workspace to a specific node.
\item Lib file: the path to a jar file containing all the necessary files to launch CArtAgO nodes. This includes custom artifacts binaries, third party libraries, custom classes binaries, and any configuration file. This file is intended to be shared among all nodes.  
\end{itemize}

The adopted way to automatically launch CArtAgO nodes in different hosts, is to have a daemon service at each host that manages the spawning and halting of them. This service works on the operating system level, and requires to be previously configured and launched, this includes security and ssh configuration. The demon also copies the lib file stated in the configuration.
    
Figure~\ref{fig:deployment} shows the stages to launch a distributed MAS. The fist three stages (from bottom to top) are part of the proposed changes, while the last ones are the same as in the current deployment.

\begin{figure}[h]
\centering
    \includegraphics[width= .75 \columnwidth]{./images/deployment}
    \caption{Stages followed to launch a distributed MAS. To deploy CArtAgO nodes, the node daemons are used.}
    \label{fig:deployment}
\end{figure}


\subsection{Connection loss}
\label{sec:dropping}

Connection loss is an important topic for distributed systems, specially for long running and mobile systems, which are part of the target of MAS. The point is to quickly notice connection loses, to be able to recuperate from one seamless, and to keep working with the available resources. Also, as network connection quality may vary, a way to reconnect should be supported as well. Given the proposed model, connection loss is the same as node dropping, and as such it directly impacts the node tree structure used by agents as all the corresponding workspaces are also lost. In what follows, the design choices related to connection loss are addressed. 

Following the overall node organization introduced in section~\ref{sec:artifacts}, all nodes maintain a keepalive connection with the central node. If a node losses connection, then the central node issues the corresponding dropping event to the rest of the nodes, and modifies the node tree structure accordingly. The disconnected node tries to establish a connection with the rest of the nodes that do not have connection with the central node, this being useful on the case that several nodes lost connection or the central node dropped. With the available nodes, a new central node is designated, issuing the corresponding disconnection events and creating a new tree node structure where every default workspace from the nodes is on the same level. The new central node keeps trying to reconnect to the original central node for a period of time. 

When successfully reconnecting, the original central node will try to remount the workspaces as they were originally, but sometimes that would not be possible, e.g.; when one of the nodes keep missed. It is strongly recommended that every default workspace corresponding to a node is mounted on the same upper tree level of the tree, that way when reconnecting, the tree structure will keep consistent with the way it was before, otherwise the tree topology may vary in an unexpected manner, which can be problematic on certain applications. After the node tree structure is recreated, the reconnecting nodes return to work with the original central node, and the central node triggers the corresponding reconnection events. 
   



  