\subsection{Formal description}
\label{sec:model}

The core elements of the proposed model are addressed in definition~\ref{def:model}, this definition focuses only on distributed $MAS$ aspects, and it does not concerns with other related aspects such as agent believes or artifact actions, to name some examples. 

\begin{mydef}
  \label{def:model} 
  A tuple $\langle Wsps, Ags, Ars, Tree, Hosts, Nodes\rangle$ is a distributed $MAS$ configuration, where:
  \begin{itemize}
  \item $Wsps = \{wsp_1, \dots,wsp_n\} (n>0)$ is the set of available workspaces. A workspace defines a logical place for artifacts and agent perception. Each $wsp_i= \langle wid, wname\rangle$ where $wid$ is an unique identifier, and $wname$ a logical name for the workspace, which could be shared.
  \item $Ags = \{ag_1,...,ag_n\} (n>0)$ is a set of agents, each one being a tuple $\langle id, JWsps \rangle$, where $id$ is an identifier for the agent, and $JWsps \subseteq Wsps$ is a set of joined workspaces $\{home, wsp_1, \dots,wsp_m\} (m>0)$ which denotes that each agent can join multiple workspaces at the same time, and no workspace is considered the current one. The workspace where the agent is originally spawned is called its home workspace, and can not change.
  \item $Ars = \{ar_1,...,ar_n\} (n>0)$ is a set of artifacts, each one being a tuple $\langle Id, wsp \rangle$ where $id$ is an identifier, and $wsp \in Wsps$ is the workspace where the artifact is situated. To execute an artifact operation, the workspace of the artifact may be specified.   
  \item $Tree = \{TN, parent\}$  is a tree structure denoting the workspace topology that follows the workspace path structure. Where $TN = \{tn_1, \dots, tn_m\}$ is a set of tree nodes, each $tn_i = \langle wsp, parent \rangle$ where $wsp \in Wsps$ representing a workspace; and $parent: TN \to TN$ is a function that indicates which tree node is the parent of a given tree node. $parent$ is anti-reflexive and anti-symmetric. Also note that only for the root node the function is undefined. Following the tree structure, workspace paths are of the form:\\
  $mainWsp/subWsp/subsubWsp/...$, from a UNIX file system like path structure. 
  \item $Nodes = \{node_1, \dots, node_k,\}$ 
is a set of CArtAgO nodes, i.e.; processes, possibly remote, where workspaces can be created. Each $node_i$ is a tuple $\langle ni, SWsps, host, port \rangle$, where $ni$ is an unique identifier for the node; $SWsps \subseteq Wsps$ is the set of spawned workspaces in the node; $host$ is an identifier of the host computer where the node exists; and $port$ is the host port used by the node process.    
  \item $Hosts = \{host_1, \dots, host_p\}$ is the set of available computer devices on the distributed system. Each $host_i$ is a tuple $\langle hi, HNodes, ip \rangle$, where $hi$ is a host unique identifier, $HNodes  \subseteq Nodes$ is a set of hosted nodes, and $ip$ is the IP address of the computer.
  \end{itemize}
\end{mydef}


Agents are only concerned about workspaces, which they manipulate though workspace paths; whereas nodes, and hosts are elements meant to be managed by a infrastructure as part of its low level services. Given a distributed MAS configuration $\langle Wsps, Ags,Ars,
Tree,Nodes,Hosts \rangle$ the set of agent actions that change the configuration, along with their corresponding semantic rules, are presented next. The rules indicate how the application of agent actions affect the MAS configuration through a transition from the current configuration to the next. To simplify, $wsp$ is always the workspace corresponding to $wspPath$, which in turn is the path of $wsp$ in the tree. If $wspPath$ is not a valid path in the tree, then $wsp$ is an empty tuple. 
\begin{itemize}
\item $joinWsp(wspPath)$ for joining an existing workspace $wsp$.
Workspace joining rule:
 \begin{equation} 
\tag{$JR_1$}
\frac{\splitfrac {ag_i=\langle Id, JWsps \rangle } {wsp \not\in JWsps \quad ag_i.joinWsp(wspPath)}} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps, Ags',Ars,Tree,Nodes,Hosts\rangle }} \end{equation}
The set of agents change because the internal state of $ag_i = \langle Id, JWsps \rangle$ changes in the following way: $ag_i = \langle Id, JWsps \cup \{wsp\} \rangle$.
 \begin{equation} 
\tag{$JR_2$}
\frac{\splitfrac {ag_i=\langle Id, JWsps \rangle } {wsp \in JWsps \quad ag_i.joinWsp(wspPath)}} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps, Ags,Ars,Tree,Nodes,Hosts\rangle }} \end{equation}

If  $ag_i$ has already joined the intended workspace, then the configuration does not change.

\item $quitWsp(wspPath)$ for quitting a workspace.
Workspace quitting rule:
 \begin{equation} 
\tag{$QR_1$}
\frac{\splitfrac {ag_i=\langle Id, JWsps \rangle } {wsp \in JWsps \quad ag_i.quitWsp(wspPath)}} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps, Ags',Ars,Tree,Nodes,Hosts\rangle }} \end{equation}

The internal state of $ag_i = \langle Id, JWsps \rangle$ changes in the following way: $ag_i = \langle Id, JWsps - \{wsp\} \rangle$

 \begin{equation} 
\tag{$QR_2$}
\frac{\splitfrac {ag_i=\langle Id, JWsps \rangle } {wsp \not \in JWsps \lor wsp = home  \quad ag_i.quitWsp(wspPath)}} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps, Ags,Ars,Tree,Nodes,Hosts\rangle }} \end{equation}

If  $ag_i$ has not joined $wsp$ or if $wsp$ is the home workspace of $ag_i$, then the configuration does not change.

\item $mount(wspPath)$ for creating a new workspace, adding it to the tree and spawning it on the corresponding node, which node depends on the infrastructure implementation, e.g.; spawn the workspace in the parent path of $wsp$.
Workspace mounting rule:

 \begin{equation} 
\tag{$MR_1$}
\frac{wsp \not \in Wsps \quad ag_i.mount(wspPath)} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps', Ags,Ars',Tree',Nodes',Hosts\rangle }} \end{equation}

$Wsps$ becomes $Wsps \cup \{wsp\}$. $Ars$ change because new default artifacts need to be created in the new workspace. A $node_i \in Nodes$ changes to $\langle ni, SWsps \cup wsp, host, port \rangle$

 \begin{equation} 
\tag{$MR_2$}
\frac{wsp  \in Wsps \quad ag_i.mount(wspPath)} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps, Ags,Ars,Tree,Nodes,Hosts\rangle }} \end{equation}

If $wsp$ already exists in $Wsps$ then the configuration does not change.

\item $unmount(wspPath)$ for unmounting an existing workspace and all its related children nodes.
Workspace unmounting rule:

 \begin{equation} 
\tag{$UR_1$}
\frac{wsp  \in Wsps \quad ag_i.unmount(wspPath)} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps', Ags',Ars',Tree',Nodes',Hosts\rangle }} \end{equation}

$wsp$ and all of its descendant workspaces are removed from $Wsps$, the removed workspaces impact the joined workspaces of the agents, and also the artifacts situated there. $Nodes$ also changes as workspaces are removed from the set. Note that a connection loss has the same impact as an unmount, with the exception that it also impacts $Hosts$.

 \begin{equation} 
\tag{$UR_2$}
\frac{wsp \not \in Wsps \quad ag_i.unmount(wspPath)} { \splitfrac {\langle Wsps, Ags,Ars,Tree,Nodes,Hosts \rangle \to} 
{\langle Wsps, Ags,Ars,Tree,Nodes,Hosts\rangle }} \end{equation}

If $wsp$ is not in $Wsps$ then the configuration does not change.

\end{itemize}










